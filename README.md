# New Year ITPU Music

New Year ITPU music

| Field                   | Value                                       |
|-------------------------|---------------------------------------------|
| Wrote and returned by   | Allayar Jandullaev                          |
| Original music link     | [All I Want For Christmas Is You](https://open.spotify.com/track/0bYg9bo50gSsH3LtXe2SQn?si=5f1a6f65d01347e6) |
| Studio                  | Garage Band                                 |
| Realized date           | 17.12.2023                                  |
| Name                    | New Year ITPU                   


Attention!
If you have some problems like music is not playing. Please text me!
Telegram account: @allayara 